import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import "./quasar";
import Axios from "axios";

Vue.config.productionTip = false;

Vue.prototype.$http = Axios;
Axios.defaults.baseURL = "http://127.0.0.1:8000/api/";
// Axios.defaults.baseURL = "http://172.104.245.14/pista_api/";

var currentUser,objCurrentUser,currentUserToken;
if (store.getters.currentUser) {
  var currentUser=store.state.currentUser;
  var objCurrentUser = JSON.parse(currentUser); 
  var currentUserToken = objCurrentUser.token;
  // console.log(objCurrentUser)
  Axios.defaults.headers.common["Authorization"] = `Bearer ${currentUserToken}`
}else{
  // console.log('not set')
}

router.beforeEach((to, from, next) => {
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth);

  if(requiresAuth && !currentUserToken) {
    next('/login');
    // next();
  } 
  // else if(to.path == '/login' || to.path == '/register' && currentUserToken) {
  else if(to.path == '/login' && currentUserToken) {
    alert("logout first")
    let userRole=objCurrentUser.role;

    if(userRole == 2) {
      next('/dashboard');
    } else if (userRole == 1) {
      next('/home');
    } else {
      next('/users');
    }
    // next();
    
  } else {
    next();
  }
  
  
});//end router before each

Axios.interceptors.response.use(null, (error) => {
  if (error.response.status == 401) {
    // store.commit('logout');
    router.push('/login');
  }
  return Promise.reject(error);
});




new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
