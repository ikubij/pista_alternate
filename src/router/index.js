import Vue from "vue";
import VueRouter from "vue-router";
import { publicRoute, protectedRoute } from "./routes";

const routes = publicRoute.concat(protectedRoute);

Vue.use(VueRouter);

const router = new VueRouter({
  mode: "history",
  // base: "/pista/",
  routes
})

router.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.forVisitors)) {
    if (Vue.auth.isAutheticated()) {
      NProgress.start();
      next({
        path: "/",
      });
    } else next();
  } else if (to.matched.some((record) => record.meta.forAuth)) {
    if (!Vue.auth.isAutheticated()) {
      NProgress.start();
      next({
        path: "/auth",
      });
    } else next();
  } else next();  
});

router.afterEach((to, from) => {
  document.title =
    to.meta && to.meta.title
      ? "P-I-S-T-A | " + to.meta.title
      : "P-I-S-T-A | Home";
  next();
});

export default router;
