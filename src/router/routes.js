import { AuthLayout, DefaultLayout, AdminLayout } from "@/components/layouts";

export const publicRoute = [
         {
           path: "*",
           component: () => import("@/views/errors/NotFound.vue"),
         },
         {
           path: "/",
           component: AuthLayout,
           meta: { title: "Join Community" },
           redirect: "/",
           hidden: true,
           children: [
             {
               path: "/",
               name: "Karibu",
               meta: { title: "Join Community" },
               component: () => import("@/views/pages/Karibu.vue"),
             },
           ],
         },
         {
           path: "/home",
           component: DefaultLayout,
           meta: { title: "Home" },
           redirect: "/home",
           hidden: true,
           children: [
             {
               path: "/home",
               name: "Home",
               meta: { title: "Home" },
               component: () => import("@/views/Home.vue"),
             },
           ],
         },
         {
           path: "/login",
           component: AuthLayout,
           meta: { title: "Login" },
           redirect: "/login",
           hidden: true,
           children: [
             {
               path: "/login",
               name: "login",
               meta: { title: "Login" },
               component: () => import("@/views/auth/Login.vue"),
             },
           ],
         },
         {
          path: "/logout",
          component: AuthLayout,
          meta: { title: "Logout" },
          redirect: "/logout",
          hidden: true,
          children: [
            {
              path: "/logout",
              name: "logout",
              meta: { title: "Logout" },
              component: () => import("@/views/auth/Logout.vue"),
            },
          ],
        },
         {
           path: "/register",
           component: AuthLayout,
           meta: { title: "Register" },
           redirect: "/register",
           hidden: true,
           children: [
             {
               path: "/register",
               name: "register",
               meta: { title: "Register" },
               component: () => import("@/views/auth/Register.vue"),
             },
           ],
         },
         //   {
         //     path: "/dashboard",
         //     component: DefaultLayout,
         //     meta: { title: "Dashboard" },
         //     redirect: "/dashboard",
         //     children: [
         //       {
         //         path: "/dashboard",
         //         name: "Dashboard",
         //         meta: { title: "Dashboard" },
         //         component: () => import("@/views/Home.vue"),
         //       },
         //     ],
         //   },
         {
           path: "/settings",
           component: DefaultLayout,
           meta: { title: "Settings" },
           redirect: "/settings",
           children: [
             {
               path: "/settings",
               name: "Settings",
               meta: { title: "Settings" },
               component: () => import("@/views/pages/Settings.vue"),
             },
           ],
         },
         {
           path: "/reports",
           component: DefaultLayout,
           meta: { title: "Reports" },
           redirect: "/reports",
           children: [
             {
               path: "/reports",
               name: "Reports",
               meta: { title: "Reports" },
               component: () => import("@/views/pages/Reports.vue"),
             },
           ],
         },
         //   {
         //     path: "/users",
         //     component: DefaultLayout,
         //     meta: { title: "User" },
         //     redirect: "/users",
         //     children: [
         //       {
         //         path: "/users",
         //         name: "User",
         //         meta: { title: "User" },
         //         component: () => import("@/views/Users.vue"),
         //       },
         //     ],
         //   },
         {
           path: "/managers",
           component: DefaultLayout,
           meta: { title: "Managers" },
           redirect: "/managers",
           children: [
             {
               path: "/managers",
               name: "Managers",
               meta: { title: "Managers" },
               component: () => import("@/views/pages/Managers.vue"),
             },
           ],
         },
         {
           path: "/departments",
           component: DefaultLayout,
           meta: { title: "Department" },
           redirect: "/departments",
           children: [
             {
               path: "/departments",
               name: "Department",
               meta: { title: "Department" },
               component: () => import("@/views/pages/Depts.vue"),
             },
           ],
         },
       ];

export const protectedRoute = [
//   {
//     path: "/admin",
//     component: AdminLayout,
//     meta: { title: "Administration", forAuth: true },
//     redirect: "/admin",
//     children: [
//       {
//         path: "/admin",
//         name: "Admin Home",
//         meta: { title: "Administration" },
//         component: () => import("@/views/admin/Dashboard.vue"),
//       },
//     ],
//   },
];
