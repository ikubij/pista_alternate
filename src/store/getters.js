let getters = {
    /**Authentication */
    isLoading(state) {
        return state.loading;
    },
    isLoggedIn(state) {
        return state.isLoggedIn;
    },
    currentUser(state) {
        return state.currentUser;
    },
    authError(state) {
        return state.auth_error;
    },

    departments(state) {
        return state.departments;
    },

    managers(state) {
        return state.managers;
    }
}
export default  getters