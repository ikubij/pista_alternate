let mutations = {
    /**Authentication */
    LoginUser (state, data) {
        state.isLoggedIn = true;
        // let token = data.access_token;
        // state.token = token;
        // localStorage.setItem('token', token);
        state.currentUser = Object.assign({}, data.user, {token: data.access_token});
        localStorage.setItem("user", JSON.stringify(state.currentUser));
    },
    LogoutUser (state) {
        state.isLoggedIn = false;
        // state.token = localStorage.removeItem('token')
        state.currentUser = null;
        localStorage.removeItem('user')
    },
    // tokenStored (state) {
    //     state.token = localStorage.getItem('token')
    // },
    currentUserStored (state) {
        state.currentUser = localStorage.getItem('user')
    },

    updateDepartments(state, payload) {
        state.departments = payload;
    },

    updateManagers(state, payload) {
        state.managers = payload;
    }
}
export default mutations