let state = {
    /***Authentication */
    currentUser: localStorage.getItem('user'),
    isLoggedIn: !!localStorage.getItem('user'),
    // token: localStorage.getItem('token'),
    departments: [],
    managers: []
}

export default state
