module.exports = {
  publicPath: "",
  pages: {
    index: {
      entry: "src/main.js",
      chunks: ["chunk-vendors", "chunk-common", "index"],
    },
  },
  pluginOptions: {
    quasar: {
      importStrategy: "kebab",
      rtlSupport: true,
    },
  },
  transpileDependencies: ["quasar"],
};
